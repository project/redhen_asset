<?php

/**
 * @file
 * Contains \Drupal\redhen_asset\AssetTypeInterface.
 */

namespace Drupal\redhen_asset;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Asset type entities.
 */
interface AssetTypeInterface extends ConfigEntityInterface {
  // Add get/set methods for your configuration properties here.

}
