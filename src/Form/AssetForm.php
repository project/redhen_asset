<?php

/**
 * @file
 * Contains \Drupal\redhen_asset\Form\AssetForm.
 */

namespace Drupal\redhen_asset\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for Asset edit forms.
 *
 * @ingroup redhen_asset
 */
class AssetForm extends ContentEntityForm {
  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, EntityInterface $host = NULL, $redhen_asset_type = NULL) {
    /* @var $entity \Drupal\redhen_asset\Entity\Asset */
    $form = parent::buildForm($form, $form_state);
    $entity = $this->entity;

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;
    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        \Drupal::messenger()->addMessage($this->t('Created %label.', [
          '%label' => $entity->label(),
        ]));
        break;

      default:
        \Drupal::messenger()->addMessage($this->t('Saved %label.', [
          '%label' => $entity->label(),
        ]));
    }

    if (isset($entity->field_recipient->target_id)) {
      $host = $entity->field_recipient->target_id;
      $type = 'redhen_contact';
    }
    elseif (isset($entity->field_organization->target_id)) {
      $host = $entity->field_organization->target_id;
      $type = 'redhen_org';
    }
    if (isset($host)) {
      $form_state->setRedirect("entity.$type.canonical", [$type => $host]);
    }
    else {
      $form_state->setRedirect("view.offline_payments.page_1");
    }
  }

}
